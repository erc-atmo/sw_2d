import numpy                   as np
import matplotlib              as mpl
import matplotlib.font_manager as mpfm
import matplotlib.pyplot       as plt

from pylab import *

plt.style.use('ggplot')
plt.style.use('bmh')
prop = mpfm.FontProperties(size=12)
mpl.rc('text',usetex=False)    
mpl.rc('font',family='serif',size=12)

def read_output(fin='output.txt',fout='output.pdf',nx=3000,ny=3000):

    ff_1d = np.loadtxt(fin,skiprows=2)

    ff_2d = np.zeros((nx+2,ny+2))
    
    for ix in range(nx+2):
        for iy in range(ny+2):
            ff_2d[ix,iy] = ff_1d[iy+ix*(ny+2)]

    figure(1,figsize=(6,4))
    clf()
    
    imshow(ff_2d,interpolation='nearest')
    colorbar()
    savefig(fout)
    
read_output()
