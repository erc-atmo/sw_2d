//@HEADER
// ************************************************************************
// 
//            sw_2d: Saturated Water in 2d (single-phase flow)
//                 
//   This example is a simple test for a diffusion equation for single
// phase flows aiming at taking advantage of the Trilinos Library to
// solve the resulting linear system on MPI+X (X=OpenMP, Cuda) 
// architectures.
// This example is a direct adapatation of the Trilinos tutorial
// Tpetra_Belos_CreateSolver available at:
// github.com/trilinos/Trilinos_tutorial/wiki/Tpetra_Belos_CreateSolver
// but includes also the use of an algebraic multigrid preconditioner
// available in the MueLu package.
//
// Questions? Contact Pascal Tremblin (pascal.tremblin@cea.fr) 
// 
// ************************************************************************
//@HEADER

#include <iostream>
#include <Teuchos_GlobalMPISession.hpp>
#include <Teuchos_oblackholestream.hpp>
#include <Teuchos_TimeMonitor.hpp>
#include <Teuchos_ParameterList.hpp>
#include <Teuchos_YamlParameterListCoreHelpers.hpp>
#include <Kokkos_Core.hpp>
#include <Tpetra_Core.hpp>
#include <Tpetra_CrsMatrix.hpp>
#include <Tpetra_Version.hpp>
#include <MatrixMarket_Tpetra.hpp>
#include <BelosTpetraAdapter.hpp>
#include <BelosSolverFactory.hpp>
#include <MueLu.hpp>
#include <MueLu_CreateTpetraPreconditioner.hpp>
#include <BelosMueLuAdapter.hpp>

// Create and return a simple CrsMatrix for the diffusion equation.
template<class TpetraMatrixType>
Teuchos::RCP<TpetraMatrixType>
createMatrix (const Teuchos::RCP<const Teuchos::Comm<int> >& comm,
              Teuchos::ParameterList& InputsParam)
{
  using Teuchos::arcp;
  using Teuchos::ArrayRCP;
  using Teuchos::ArrayView;
  using Teuchos::RCP;
  using Teuchos::rcp;
  using Teuchos::Time;
  using Teuchos::TimeMonitor;
  using Teuchos::tuple;

  typedef TpetraMatrixType matrix_type;

  // Fetch typedefs from the Tpetra::CrsMatrix.
  typedef typename TpetraMatrixType::scalar_type scalar_type;
  typedef typename TpetraMatrixType::local_ordinal_type local_ordinal_type;
  typedef typename TpetraMatrixType::global_ordinal_type global_ordinal_type;
  typedef typename TpetraMatrixType::node_type node_type;

  // The type of the Tpetra::Map that describes how the matrix is distributed.
  typedef Tpetra::Map<local_ordinal_type, global_ordinal_type, node_type> map_type;

  // Input Parameters
  size_t nx = InputsParam.get<int>("nx");
  size_t ny = InputsParam.get<int>("ny");
 
  const scalar_type box_size    = InputsParam.get<double>("box_size"   );
  const scalar_type csat        = InputsParam.get<double>("csat"       );
  const scalar_type cfl         = InputsParam.get<double>("cfl"        ); 
  const scalar_type kappa_high  = InputsParam.get<double>("kappa_high" );
  const scalar_type kappa_low   = InputsParam.get<double>("kappa_low"  );

  const scalar_type dx = box_size/nx;
  const scalar_type dt = csat*dx*dx/kappa_high*cfl;
  
  // The global number of rows in the matrix A to create.  
  const Tpetra::global_size_t numGlobalElements = (nx+2)*(ny+2);
  
  // Construct a Map that puts approximately the same number of
  // equations on each processor.
  const global_ordinal_type indexBase = 0;
  RCP<const map_type > map = 
    rcp (new map_type (numGlobalElements, indexBase, comm, 
                       Tpetra::GloballyDistributed));

  // Get update list and the number of equations that this MPI process
  // owns.
  const size_t numMyElements = map->getNodeNumElements();
  ArrayView<const global_ordinal_type> myGlobalElements = map->getNodeElementList();

  // NumNz[i] will be the number of nonzero entries for the i-th
  // global equation on this MPI process.
  ArrayRCP<size_t> NumNz = arcp<size_t> (numMyElements);


  // global indices in 2D
  size_t i_glob = 0;
  size_t j_glob = 0;
  global_ordinal_type myGlobEl_loc  = 0;
  global_ordinal_type myGlobEl_nei  = 0;
  
  // We are building diffusion problem,
  // so we need 4 off-diagonal terms (except for boundaries only 1).
  for (size_t i = 0; i < numMyElements; ++i) {
   
    myGlobEl_loc = myGlobalElements[i];
    
    i_glob = myGlobEl_loc/(ny+2);
    j_glob = myGlobEl_loc - i_glob*(ny+2);

    //boundaries:
    //x left boundary + corner
    if (i_glob==0) 
      {
	NumNz[i] = 2;
      }
    //x right boundary + corner
    else if (i_glob==nx+1) 
      {

	NumNz[i] = 2;
	
      }
    //y left boundary - corner
    else if (j_glob == 0 and i_glob>0 and i_glob<nx+1) 
      {

	NumNz[i] = 2;

      }
    //y right boundary - corner
    else if (j_glob == ny+1 and i_glob>0 and i_glob<nx+1) 
      {

	NumNz[i] = 2;

      }
    // inner domain
    else  
      {

	NumNz[i] = 5;
	
      }
    
  }

  // Create a Tpetra::Matrix using the Map, with a static allocation
  // dictated by NumNz.  (We know exactly how many elements there will
  // be in each row, so we use static profile for efficiency.)
  RCP<matrix_type> A = rcp (new matrix_type (map, NumNz(), Tpetra::StaticProfile));
  
  // We are done with NumNZ; free it.
  NumNz = Teuchos::null;

  // Add rows one at a time.  Off diagonal values will always be -1.
  const scalar_type negOne = static_cast<scalar_type> (-1.0);
  const scalar_type One    = static_cast<scalar_type> ( 1.0);

  scalar_type kappa_loc   = 0.0;
  scalar_type kappa_nei   = 0.0;
  scalar_type kappa_face  = 0.0;
  scalar_type Aii         = 0.0;
  scalar_type Ele_loc     = 0.0;
  
  for (size_t i = 0; i < numMyElements; i++) {
   
    myGlobEl_loc = myGlobalElements[i];
    
    i_glob = myGlobEl_loc/(ny+2);
    j_glob = myGlobEl_loc - i_glob*(ny+2);
    
    if (i_glob==0) //x left boundary + corner
      {
	
	myGlobEl_nei = j_glob + 1*(ny+2);
      
	A->insertGlobalValues (myGlobEl_loc,tuple(myGlobEl_loc,myGlobEl_nei),
			       tuple(One,negOne));
      
      }
    else if (i_glob==nx+1) //x right boundary + corner
      {
	
	myGlobEl_nei = j_glob + nx*(ny+2);

	A->insertGlobalValues (myGlobEl_loc,tuple(myGlobEl_loc,myGlobEl_nei),
			       tuple(One,negOne));
	
      }
    else if (j_glob == 0 and i_glob>0 and i_glob<nx+1) //y left boundary - corner
      {
		
	myGlobEl_nei = 1 + i_glob*(ny+2);

	A->insertGlobalValues (myGlobEl_loc,tuple(myGlobEl_loc,myGlobEl_nei),
			       tuple(One,negOne));

      }
    else if (j_glob == ny+1 and i_glob>0 and i_glob<nx+1) //y right boundary - corner
      {

	myGlobEl_nei = ny + i_glob*(ny+2);

	A->insertGlobalValues (myGlobEl_loc,tuple(myGlobEl_loc,myGlobEl_nei),
			       tuple(One,negOne));

      }
    else  // inner domain
      {

	Aii = One;
	kappa_loc = (j_glob<i_glob  +ny/6 and j_glob>=i_glob  )? kappa_high : kappa_low ;
	
	//left x gradient
	kappa_nei = (j_glob<i_glob-1+ny/6 and j_glob>=i_glob-1)? kappa_high : kappa_low ;
	kappa_face = 0.5*(kappa_loc+kappa_nei);
	
	myGlobEl_nei = j_glob + (i_glob-1)*(ny+2);

	Ele_loc = kappa_face/dx/dx*dt/csat;
	Aii += Ele_loc;

	A->insertGlobalValues (myGlobEl_loc,tuple(myGlobEl_nei),tuple(-Ele_loc));

	//right x gradient
	kappa_nei = (j_glob<i_glob+1+ny/6 and j_glob>=i_glob+1)? kappa_high : kappa_low ;
	kappa_face = 0.5*(kappa_loc+kappa_nei);
	
	myGlobEl_nei = j_glob + (i_glob+1)*(ny+2);

	Ele_loc = kappa_face/dx/dx*dt/csat;
	Aii += Ele_loc;
	A->insertGlobalValues (myGlobEl_loc,tuple(myGlobEl_nei),tuple(-Ele_loc));
	
	//left y gradient
	kappa_nei = (j_glob-1<i_glob+ny/6 and j_glob-1>=i_glob)? kappa_high : kappa_low ;
	kappa_face = 0.5*(kappa_loc+kappa_nei);
	
	myGlobEl_nei = j_glob-1 + i_glob*(ny+2);
	
	Ele_loc = kappa_face/dx/dx*dt/csat;
	Aii += Ele_loc;
	A->insertGlobalValues (myGlobEl_loc,tuple(myGlobEl_nei),tuple(-Ele_loc));

	//right y gradient
	kappa_nei = (j_glob+1<i_glob+ny/6 and j_glob+1>=i_glob)? kappa_high : kappa_low ;
	kappa_face = 0.5*(kappa_loc+kappa_nei);
	
	myGlobEl_nei = j_glob+1 + i_glob*(ny+2);

	Ele_loc = kappa_face/dx/dx*dt/csat;
	Aii += Ele_loc;
	A->insertGlobalValues (myGlobEl_loc,tuple(myGlobEl_nei),tuple(-Ele_loc));

	A->insertGlobalValues (myGlobEl_loc,tuple(myGlobEl_loc),tuple(Aii));
	
      }
    
  }

  // Finish up the matrix.
  A->fillComplete ();
  return A;
}

int 
main (int argc, char *argv[]) 
{
  using std::endl;
  using Teuchos::RCP;
  using Teuchos::rcp;
  using Teuchos::ParameterList;
  using Teuchos::Time;
  using Teuchos::TimeMonitor;
  using Teuchos::ArrayView;
  
  Tpetra::ScopeGuard tpetraScope (&argc, &argv);

  {
      RCP<const Teuchos::Comm<int> > comm = Tpetra::getDefaultComm();

      // Set up Tpetra typedefs.
      typedef Tpetra::Vector<>::scalar_type scalar_type;
      typedef Tpetra::Vector<>::local_ordinal_type local_ordinal_type;
      typedef Tpetra::Vector<>::global_ordinal_type global_ordinal_type;
      typedef Tpetra::Vector<>::node_type node_type;

      // get MPI rank size, and set out-to-terminal process
      const int myRank = comm->getRank();
      const int numProcs = comm->getSize();

      Teuchos::oblackholestream blackHole;
      std::ostream& out = (myRank == 0) ? std::cout : blackHole;
 
      // Run the whole example: create the sparse matrix, and compute the preconditioner.
      // Print out the Tpetra software version information.
      out << Tpetra::version() << endl << endl;

      //Kokkos configuration
      out << "Kokkos configuration" << endl;
      if ( Kokkos::hwloc::available() ) {
          out << "hwloc( NUMA[" << Kokkos::hwloc::get_available_numa_count()
              << "] x CORE["    << Kokkos::hwloc::get_available_cores_per_numa()
              << "] x HT["      << Kokkos::hwloc::get_available_threads_per_core()
              << "] )"
              << endl ;
      }

      Kokkos::print_configuration(out);
  
      // Read all parameters
      std::string YamlInputFile;
      if (argv[1]!=NULL) {
          YamlInputFile = std::string(argv[1]);
      } else {
          YamlInputFile = "inputs.yml";
      }

      RCP<ParameterList> AllParam = Teuchos::getParametersFromYamlFile(YamlInputFile);

      // Print name of input file and all parameters
      out << "Input file: " << YamlInputFile << endl;
      AllParam->print(out,1,true,false);
  
      // Get references to specific parameter lists
      ParameterList& InputsParam = AllParam->get<ParameterList>("Inputs");
      ParameterList& BelosParam  = AllParam->get<ParameterList>("Belos");
      ParameterList& MueluParam  = AllParam->get<ParameterList>("MueLu");

      typedef Tpetra::CrsMatrix<scalar_type, local_ordinal_type, global_ordinal_type, node_type> matrix_type;
      typedef Tpetra::Operator<scalar_type, local_ordinal_type, global_ordinal_type, node_type> op_type;
      typedef Tpetra::MultiVector<scalar_type, local_ordinal_type, global_ordinal_type, node_type> vec_type;
      typedef Tpetra::Vector<scalar_type, local_ordinal_type, global_ordinal_type, node_type> simple_vec_type;
      typedef MueLu::TpetraOperator<scalar_type,local_ordinal_type,global_ordinal_type,node_type> muelu_op_type;
      typedef Tpetra::MatrixMarket::Writer<vec_type> Writer;
 
      RCP<Time> matrixTimer = 
          TimeMonitor::getNewCounter ("Matrix creation");
      matrixTimer->start();

      // Create an example sparse matrix.
      RCP<matrix_type> A = createMatrix<matrix_type> (comm, InputsParam);

      matrixTimer->stop();

      // Create vectors ("multivectors" may store one or more vectors)  Set to zeros by default. 
      RCP<vec_type> X  = rcp (new vec_type (A->getDomainMap (), 1));
      RCP<vec_type> B  = rcp (new vec_type (A->getRangeMap  (), 1));
      RCP<vec_type> Qw = rcp (new vec_type (A->getRangeMap  (), 1));
      RCP<simple_vec_type> Mask = rcp (new simple_vec_type (A->getRangeMap (), 1)); 

      //initialization
      const scalar_type initial_value = static_cast<scalar_type> (1.0);

      X->putScalar(initial_value);
      Qw->putScalar(0.);
      B->putScalar(0.);
      Mask->putScalar(initial_value);

      size_t nx = InputsParam.get<int>("nx");
      size_t ny = InputsParam.get<int>("ny");
      size_t nt = InputsParam.get<int>("nt");

      const scalar_type box_size    = InputsParam.get<double>("box_size"   );
      const scalar_type csat        = InputsParam.get<double>("csat"       );
      const scalar_type cfl         = InputsParam.get<double>("cfl"        ); 
      const scalar_type kappa_high  = InputsParam.get<double>("kappa_high" );
      const scalar_type source_rate = InputsParam.get<double>("source_rate");

      const scalar_type dx   = box_size/nx;
      const scalar_type dt   = csat*dx*dx/kappa_high*cfl;
  
      size_t i_glob = 0;
      size_t j_glob = 0;
      global_ordinal_type myGlobEl_loc  = 0;

      //get the number of elements
      const size_t numMyElements = A->getRangeMap()->getNodeNumElements();
      ArrayView<const global_ordinal_type> myGlobalElements =  A->getRangeMap()->getNodeElementList();
  
      for (size_t i = 0; i < numMyElements; ++i) {
  
          myGlobEl_loc = myGlobalElements[i];
    
          i_glob = myGlobEl_loc/(ny+2);
          j_glob = myGlobEl_loc - i_glob*(ny+2);

          if (i_glob==0) //x left boundary + corner
          {
              Mask->replaceGlobalValue(myGlobEl_loc,0.);
          }
          else if (i_glob==nx+1) //x right boundary + corner
          {
              Mask->replaceGlobalValue(myGlobEl_loc,0.);
          }
          else if (j_glob == 0 and i_glob>0 and i_glob<nx+1) //y left boundary - corner
          {
              Mask->replaceGlobalValue(myGlobEl_loc,0.);
          }
          else if (j_glob == ny+1 and i_glob>0 and i_glob<nx+1) //y right boundary - corner
          {
              Mask->replaceGlobalValue(myGlobEl_loc,0.);
          }

          //source term
          if (4*i_glob==nx and 4*j_glob==ny){

              Qw->replaceGlobalValue(myGlobEl_loc,0,source_rate*dt/csat);
      
          }
    
      }

      //update RHS
      B->assign(*Qw);
      B->elementWiseMultiply(1.,*Mask,*X,1.);

      std::string solver_type = InputsParam.get<std::string>("solver_type");

      // get factory
      Belos::SolverFactory<scalar_type, vec_type, op_type> factory;
      RCP<Belos::SolverManager<scalar_type, vec_type, op_type> > solver = 
          factory.create (solver_type, rcpFromRef(BelosParam));

      // Create a LinearProblem struct with the problem to solve.
      // A, X, B, and M are passed by (smart) pointer, not copied.
      typedef Belos::LinearProblem<scalar_type, vec_type, op_type> problem_type;
      RCP<problem_type> problem = 
          rcp (new problem_type (A, X, B));

      const std::string precond = InputsParam.get<std::string>("precond","none");

      if (precond=="left") {
          // Create AMG Preconditionner.  
          RCP<Time> precondTimer = 
              TimeMonitor::getNewCounter ("Preconditioner creation");
          precondTimer->start();
    
          Teuchos::RCP<op_type> A_op(A);
          RCP<muelu_op_type> M = MueLu::CreateTpetraPreconditioner<scalar_type,local_ordinal_type,global_ordinal_type,node_type>(A_op,MueluParam);
    
          precondTimer->stop();
    
          // You don't have to call this if you don't have a preconditioner.
          // If M is null, then Belos won't use a (left) preconditioner.
          problem->setLeftPrec(M);
      } else if (precond=="right") {
          // Create AMG Preconditionner.  
          RCP<Time> precondTimer = 
              TimeMonitor::getNewCounter ("Preconditioner creation");
          precondTimer->start();
    
          Teuchos::RCP<op_type> A_op(A);
          RCP<muelu_op_type> M = MueLu::CreateTpetraPreconditioner<scalar_type,local_ordinal_type,global_ordinal_type,node_type>(A_op,MueluParam);
    
          precondTimer->stop();
    
          // You don't have to call this if you don't have a preconditioner.
          // If M is null, then Belos won't use a (left) preconditioner.
          problem->setRightPrec(M);

      }

      // Tell the LinearProblem to make itself ready to solve.
      problem->setProblem ();
  
      // Tell the solver what problem you want to solve.
      solver->setProblem (problem);
    
      // Timer to measure the loop time
      RCP<Time> loopTimer = 
          TimeMonitor::getNewCounter ("Loop time");
      loopTimer->start();

      // time loop
      for (size_t it = 0; it < nt; ++it) {
   
          // Attempt to solve the linear system.  result == Belos::Converged 
          // means that it was solved to the desired tolerance.  This call 
          // overwrites X with the computed approximate solution.
          Belos::ReturnType result = solver->solve();
   
          // Ask the solver how many iterations the last solve() took.
          const int numIters = solver->getNumIters();

          const scalar_type achievedTol = solver->achievedTol();
  
          if (result == Belos::Converged) {
              out << "The Belos solve took " << numIters << " iteration(s) to reach "
                  "a relative residual tolerance of " << achievedTol << "." << std::endl;
          } else {
              out << "The Belos solve took " << numIters << " iteration(s), but did not reach " 
                  "convergence, relative residual tolerance of " << solver->achievedTol() 
                  << ", loss of accuracy: "<<  solver->isLOADetected() << "."  << std::endl;
          }

          // update RHS
          B->assign(*Qw);
          B->elementWiseMultiply(1.,*Mask,*X,1.);

          // Tell the linear solver to (re)prepare the problem to solve.
          // This computes, for example, the initial residual vector(s).
          solver->reset(Belos::Problem);

      }

      loopTimer->stop();

      //write output in a txt file
      const std::string output_name = InputsParam.get<std::string>("output_name","output.txt");
      const bool output = InputsParam.get<bool>("output",false);

      if (output) {
        Writer::writeDenseFile(output_name, *X);
      }

      // Summarize global performance timing results, for all timers
      // created using TimeMonitor::getNewCounter().
      TimeMonitor::summarize(out);
  }

  return 0;
}
