# sw_2d

## Purpose
sw_2d: Saturated Water in 2d (single-phase flow)
                 
This example is a simple test for a diffusion equation for single phase flows aiming at taking advantage of the Trilinos Library to solve the resulting linear system on MPI+X (X=OpenMP, Cuda) architectures.
This example is a direct adapatation of the Trilinos tutorial [Tpetra_Belos_CreateSolver](https://github.com/trilinos/Trilinos_tutorial/wiki/Tpetra_Belos_CreateSolver) but includes also the use of an algebraic multigrid preconditioner available in the MueLu package.

### Dependencies
On Poincare (cluster at Maison de la Simulation)

* add a .modulerc file in your home containing:
```
#%Module1.0
# ~/.modulerc
module use /gpfshome/mds/staff/ptremblin/modulefiles
```
* module load cmake/3.4.3 gnu-env/4.8.4 openmpi/1.8.4_gnu47 cuda/7.5 trilinos/12.10.1-gnu48_openmpi

### Build instruction
```shell
git clone https://gitlab.erc-atmo.eu/erc-atmo/sw_2d.git
cd sw_2d
mkdir build ; cd build
module load cmake/3.4.3 gnu-env/4.8.4 openmpi/1.8.4_gnu47 cuda/7.5 trilinos/12.10.1-gnu48_openmpi
cmake -DUSE_CUDA=ON .. #USE_CUDA is off by default -> openMP
make
```

### Run on Poincare
* e.g. interactively on 2 GPUs (2 tasks per node mapped with two GPUs per node)
```shell
llinteractive 1 clgpu 1
module load cmake/3.4.3 gnu-env/4.8.4 openmpi/1.8.4_gnu47 cuda/7.5 trilinos/12.10.1-gnu48_openmpi
mpirun -np 2 --map-by socket --bind-to socket ./sw_2d ../inputs.yml --kokkos-ndevices=2 > run.log
```

* e.g. interactively with openMP (2 tasks per node mapped with two sockets per node)
```shell
llinteractive 1 clallmds+ 1
module load cmake/3.4.3 gnu-env/4.8.4 openmpi/1.8.4_gnu47 cuda/7.5 trilinos/12.10.1-gnu48_openmpi
export TASKS_PER_NODE=2
export OMP_NUM_THREADS=8
mpirun -np 2 --map-by socket --bind-to socket ./sw_2d ../inputs.yml > run.log
```

### Read output
if ouput=true in inputs.yml (false by default)
```shell
module load python
python ../read_ouput.py
```
